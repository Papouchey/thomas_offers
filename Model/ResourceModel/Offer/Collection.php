<?php

namespace Thomasc\Offers\Model\ResourceModel\Offer;


use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

/**
 * Contact Resource Model Collection
 *
 * @author      Pierre FAY
 */
class Collection extends AbstractCollection
{
    /**
     * Initialize resource collection
     *
     * @return void
     */
    public function _construct()
    {
        $this->_init('Thomasc\Offers\Model\Offer', 'Thomasc\Offers\Model\ResourceModel\Offer');
    }
}

