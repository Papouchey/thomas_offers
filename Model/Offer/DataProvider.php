<?php
namespace Thomasc\Offers\Model\Offer;
use Thomasc\Offers\Model\ResourceModel\Offer\CollectionFactory;
use Thomasc\Offers\Model\ResourceModel\OfferCategory\CollectionFactory as CollectionOfferCategoryFactory;
class DataProvider extends \Magento\Ui\DataProvider\AbstractDataProvider
{
    protected $_storeManager;

    /**
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param CollectionFactory $offerCollectionFactory
     * @param CollectionOfferCategoryFactory $offerCategoryCollectionFactory
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param array $meta
     * @param array $data
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        CollectionFactory $offerCollectionFactory,
        CollectionOfferCategoryFactory $offerCategoryCollectionFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        array $meta = [],
        array $data = []
    ) {
        $this ->_storeManager = $storeManager;
        $this->collection = $offerCollectionFactory->create();
        $this->collection_offer_category = $offerCategoryCollectionFactory->create();
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
    }

    public function getData()
    {
        if (isset($this->loadedData)) {
            return $this->loadedData;
        }
        $mediaUrl = $this ->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);

        $items = $this->collection->getItems();
        $this->loadedData = array();
        foreach ($items as $offer) {
            $data = $offer->getData();
            if (isset($data['image'])) {
                $name = $data['image'];
                unset($data['image']);
                $data['image'][0] = [
                    'name' => $name,
                    'url' => $mediaUrl.'thomasc/offers/'.$name
                ];
            }


            $offerCategoryCollection = $this->collection_offer_category
                ->addFieldToFilter('id_offer', $offer->getId())
            ;
            $categories = array();
            foreach ($offerCategoryCollection as $offerCategory) {
                $categories[] = $offerCategory->getIdCategory();
            }
            $data['categories'] = $categories;
            $this->loadedData[$offer->getId()]['offer'] = $data;
        }

        return $this->loadedData;
    }
}
