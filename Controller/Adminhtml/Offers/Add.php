<?php
namespace Thomasc\Offers\Controller\Adminhtml\Offers;
use DateTime;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Thomasc\Offers\Model\ImageUploader;
use Thomasc\Offers\Model\Offer;
use Thomasc\Offers\Model\OfferCategory;

class Add extends \Magento\Backend\App\Action
{
    /**
     * Image uploader
     *
     * @var ImageUploader
     */
    protected $imageUploader;

    /**
     * Upload constructor.
     *
     * @param Context $context
     * @param ImageUploader $imageUploader
     */
    public function __construct(
        Context $context,
        ImageUploader $imageUploader
    ) {
        parent::__construct($context);
        $this->imageUploader = $imageUploader;
    }

    public function execute()
    {
        $this->_view->loadLayout();
        $this->_view->renderLayout();

        $offerDatas = $this->getRequest()->getParam('offer');
        if(is_array($offerDatas)) {
            $begin_date = explode('/' , $offerDatas['begin_date']);
            $begin_date  = $begin_date[2].'-'.$begin_date[1].'-'.$begin_date[0];

            $end_date = explode('/' , $offerDatas['end_date']);
            $end_date  = $end_date[2].'-'.$end_date[1].'-'.$end_date[0];

            $offerDatas['begin_date'] = $begin_date;
            $offerDatas['end_date'] = $end_date;


            if (isset($offerDatas['image'][0]['name']) && isset($offerDatas['image'][0]['tmp_name'])) {
                $offerDatas['image'] = $offerDatas['image'][0]['name'];
                $this->imageUploader->moveFileFromTmp($offerDatas['image']);
            } elseif (isset($offerDatas['image'][0]['name']) && !isset($offerDatas['image'][0]['tmp_name'])) {
                $offerDatas['image'] = $offerDatas['image'][0]['name'];
            } else {
                $offerDatas['image'] = '';
            }

            $offer = $this->_objectManager->create(Offer::class);
            $offer->setData($offerDatas)->save();

            foreach ($offerDatas['categories'] as $category) {
                $offerCategory = $this->_objectManager->create(OfferCategory::class);
                $data = [
                    'id_category' => $category,
                    'id_offer' => $offer->getId()
                ];
                $offerCategory->setData($data)->save();
            }

            $resultRedirect = $this->resultRedirectFactory->create();
            return $resultRedirect->setPath('*/*/index');
        }
    }
}
