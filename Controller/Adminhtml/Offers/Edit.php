<?php
namespace Thomasc\Offers\Controller\Adminhtml\Offers;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Thomasc\Offers\Model\ImageUploader;
use Thomasc\Offers\Model\Offer;
use Thomasc\Offers\Model\OfferCategory;
use Thomasc\Offers\Model\ResourceModel\OfferCategory\CollectionFactory as collectionOfferCategoryFactory;

class Edit extends \Magento\Backend\App\Action
{
    protected $resultPageFactory;

    /**
     * Image uploader
     *
     * @var ImageUploader
     */
    protected $imageUploader;

    public function __construct(
        Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\Registry $registry,
        ImageUploader $imageUploader
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->imageUploader = $imageUploader;
        parent::__construct($context);
    }

    /**
     * {@inheritdoc}
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Thomasc_Offers::edit');
    }

    /**
     * Edit Blog post
     *
     * @return \Magento\Backend\Model\View\Result\Page|\Magento\Backend\Model\View\Result\Redirect
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function execute()
    {
        $this->_view->loadLayout();
        $this->_view->renderLayout();

        $offerDatas = $this->getRequest()->getParam('offer');
        if(is_array($offerDatas)) {
            $begin_date = explode('/' , $offerDatas['begin_date']);
            $begin_date  = $begin_date[2].'-'.$begin_date[1].'-'.$begin_date[0];

            $end_date = explode('/' , $offerDatas['end_date']);
            $end_date  = $end_date[2].'-'.$end_date[1].'-'.$end_date[0];

            $offerDatas['begin_date'] = $begin_date;
            $offerDatas['end_date'] = $end_date;

            if (isset($offerDatas['image'][0]['name']) && isset($offerDatas['image'][0]['tmp_name'])) {
                $offerDatas['image'] = $offerDatas['image'][0]['name'];
                $this->imageUploader->moveFileFromTmp($offerDatas['image']);
            } elseif (isset($offerDatas['image'][0]['name']) && !isset($offerDatas['image'][0]['tmp_name'])) {
                $offerDatas['image'] = $offerDatas['image'][0]['name'];
            } else {
                $offerDatas['image'] = '';
            }

            $offer = $this->_objectManager->create(Offer::class);
            $offer->setData($offerDatas)->save();
            $this->resetOfferCategories($offer->getId());

            foreach ($offerDatas['categories'] as $category) {
                $offerCategory = $this->_objectManager->create(OfferCategory::class);
                $data = [
                    'id_category' => $category,
                    'id_offer' => $offer->getId()
                ];
                $offerCategory->setData($data)->save();
            }

            $resultRedirect = $this->resultRedirectFactory->create();
            return $resultRedirect->setPath('*/*/index');
        }
    }

    public function resetOfferCategories($id)
    {
        $offerCategories = $this->_objectManager->create(collectionOfferCategoryFactory::class)->create()
            ->addFieldToFilter('id_offer', $id);
        foreach ($offerCategories as $offerCategory) {
            $offerCategory->delete();
        }
    }
}
