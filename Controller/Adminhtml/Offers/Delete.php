<?php
namespace Thomasc\Offers\Controller\Adminhtml\Offers;
use Thomasc\Offers\Model\Offer as Offer;
use Thomasc\Offers\Model\ResourceModel\OfferCategory\CollectionFactory as collectionOfferCategoryFactory;
use Magento\Backend\App\Action;

class Delete extends \Magento\Backend\App\Action
{
    public function execute()
    {
        $id = $this->getRequest()->getParam('id');

        if (!($offer = $this->_objectManager->create(Offer::class)->load($id))) {
            $this->messageManager->addError(__('Unable to proceed. Please, try again.'));
            $resultRedirect = $this->resultRedirectFactory->create();
            return $resultRedirect->setPath('*/*/index', array('_current' => true));
        }
        try{
            $offerCategories = $this->_objectManager->create(collectionOfferCategoryFactory::class)->create()
                ->addFieldToFilter('id_offer', $id);
            foreach ($offerCategories as $offerCategory) {
                $offerCategory->delete();
            }
            $offer->delete();
            $this->messageManager->addSuccess(__('Your offer has been deleted !'));
        } catch (Exception $e) {
            $this->messageManager->addError(__('Error while trying to delete offer: '));
            $resultRedirect = $this->resultRedirectFactory->create();
            return $resultRedirect->setPath('*/*/index', array('_current' => true));
        }

        $resultRedirect = $this->resultRedirectFactory->create();
        return $resultRedirect->setPath('*/*/index', array('_current' => true));
    }
}
