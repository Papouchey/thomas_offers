<?php
namespace Thomasc\Offers\Block;
use DateTime;
use Magento\Framework\View\Element\Template;

class Offerslist extends \Magento\Framework\View\Element\Template
{
    protected $offerModelFactory;
    protected $offerCategoryModelFactory;

    public function __construct(
        Template\Context $context,
        \Thomasc\Offers\Model\ResourceModel\Offer\CollectionFactory $offerModelFactory,
        \Thomasc\Offers\Model\ResourceModel\OfferCategory\CollectionFactory $offerCategoryModelFactory,
        \Magento\Framework\App\ResourceConnection $resource,
        array $data = array()
    ) {
        $this->offerModelFactory = $offerModelFactory;
        $this->offerCategoryModelFactory = $offerCategoryModelFactory;
        parent::__construct($context, $data);
        $this->setData('offers',array());
    }

    public function getOffers()
    {
        $date = new DateTime();

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $category = $objectManager->get('Magento\Framework\Registry')->registry('current_category');//get current category

        $collection = $this->offerModelFactory
            ->create()
            ->addFieldToFilter('is_active', 1)
            ->addFieldToFilter('begin_date', array('lteq' => $date->format('Y-m-d H:i:s')))
            ->addFieldToFilter('end_date', array('gteq' => $date->format('Y-m-d H:i:s')))
        ;
        $collection->getSelect()->join(
            ['offercategory' => $collection->getTable('thomasc_offer_category')],
            'main_table.id = offercategory.id_offer',
        );

        $collection->addFieldToFilter('id_category', $category->getId());

        return $collection;
    }

    /**
     * Return Image URL with image name
     * @return string
     */
    public function getImageUrl($imageName)
    {
        $media = $this ->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
        return $media.'thomasc/offers/'.$imageName;
    }
}
